--------------------------------------------------------
-- Minetest :: Live SpawnCam Mod (spawncam)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2022, Leslie E. Krause
--
-- ./games/minetest_game/mods/spawncam/init.lua
--------------------------------------------------------

local config = minetest.load_config( )
local mod_path = minetest.get_modpath( "wieldview" )
local camera

local splash_screen = "size[36.0,20.0,false]"
	.. "bgcolor[#000000FF;true]"
	.. string.format( "label[1.2,1.2;%s]", "SPAWNCAM MOD" )
	.. string.format( "label[1.2,1.8;%s]", "VERSION 1.0" )

local function is_player_camera( player_name )
	local privs = minetest.get_player_privs( player_name )

	return privs.camera and not privs.server
end

local function HiddenCamera( self )
	local self = self or { }
	local player_name
	local last_num
	local timeout

	local function get_formspec( )
		local formspec = "size[36.0,20.0,false]"
--			.. "bgcolor[#0000DD77;true]"
			.. "bgcolor[#00000000;true]"
			.. "box[25.0,1.0;10.0,0.8;#000000]"
			.. "box[32.0,1.0;3.0,0.8;#DD0000]"
			.. "box[25.0,1.0;7.0,0.8;#000000]"
			.. string.format( "label[25.3,1.2;%s]", string.upper( config.viewpoints[ last_num ].title ) )
			.. string.format( "label[32.3,1.2;CAMERA #%d]", last_num )
			.. "box[27.0,18.0;8.0,0.8;#000000]"
			.. "box[27.0,18.0;8.0,0.8;#000000]"
			.. string.format( "label[27.3,18.2;%s]", minetest.get_date_string( "DAY %d, MONTH %m, YEAR %y " ) )
			.. string.format( "label[33.0,18.2;%s]", minetest.get_time_string( "%cc:%mm %pp" ) )
		return formspec
	end

	self.move_to_prev = function ( self, speed, is_tween )
		self:jump_to( last_num > 1 and last_num - 1 or 1, speed, is_tween )
	end

	self.move_to_next = function ( self, speed, is_tween )
		self:move_to( last_num < #config.viewpoints and last_num + 1 or 1, speed, is_tween )
	end

	self.jump_to_next = function ( self )
		self:jump_to( last_num < #config.viewpoints and last_num + 1 or 1 )
	end

	self.jump_to_prev = function ( self )
		self:jump_to( last_num > 1 and last_num - 1 or #config.viewpoints )
	end

	self.jump_to = function ( self, num )
		local v = config.viewpoints[ num ]

		self.object:set_pos( v.pos )
		self.player:set_look_yaw( v.yaw )
		self.player:set_look_pitch( v.pitch )

		last_num = num
	end

	self.on_destruct = function ( self )
		for k, v in pairs( config.viewpoints ) do
			minetest.forceload_free_block( v.pos )  
		end
	end

	self.on_construct = function ( self, player )
		player_name = player:get_player_name( )

		player:set_hp( 20 )
		player:setpos( vector.origin )
		player:hud_set_flags( { hotbar = false, healthbar = false, crosshair = false, wielditem = false, minimap = false } )
		player:set_armor_groups( { fleshy = 0, immortal = 1 } )
		player:set_eye_offset( vector.new( 0, 0, 0 ), vector.origin )
		player:set_attach( self.object, "", vector.origin, vector.origin )
		default.player_attached[ player_name ] = true

		-- forceload mapblocks so they are always available
		-- (this also resolves occasional player detachment)
		for k, v in pairs( config.viewpoints ) do
			minetest.forceload_block( v.pos )  
		end

		self.player = player

                minetest.create_form( nil, player_name, splash_screen, function ( meta, player, fields )
                        if fields.quit == minetest.FORMSPEC_SIGTIME then
				timeout = timeout - 1
				if timeout == 0 then
					if not last_num then
						self:jump_to( 1 )
					else
						config.viewpoints[ last_num ].on_exit( self )
					end
					timeout = config.viewpoints[ last_num ].timeout
				end

                                minetest.update_form( player, get_formspec( ) )

			elseif fields.key_up and last_num then
				self:jump_to_next( )
				timeout = config.viewpoints[ last_num ].timeout

                                minetest.update_form( player, get_formspec( ) )

			elseif fields.key_down and last_num then
				self:jump_to_prev( )
				timeout = config.viewpoints[ last_num ].timeout

                                minetest.update_form( player, get_formspec( ) )

			end
		end )

		minetest.after( 0.0, function ( )
			player:set_nametag_attributes( { color = { a = 0, r = 255, g = 255, b = 255 } } )
			default.player_set_textures( player, {
				"blank.png",
			} )
		end )

		minetest.get_form_timer( player_name ).start( 4.0 )
		timeout = 1
	end

	self.move_to = function ( self, num )
	end
end

---------------------------
-- Registered Privileges --
---------------------------

minetest.register_privilege( "camera", {
        description = "Join game without HUD elements (for recording and streaming).",
        give_to_singleplayer = true
} )

-------------------------
-- Registered Entities --
-------------------------

minetest.register_entity( "spawncam:hologram", {
	physical = false,
	collide_with_objects = false,
	static_save = false,
	description = "GenericSAO",
	collisionbox = { -0.3, -0.3, -0.3, 0.3, 0.3, 0.3 },
	visual = "upright_sprite",
	visual_size = { x = 1.0, y = 1.0 },
	textures = { "blank.png", "blank.png" },
	drawtype = "front",
	makes_footstep_sound = false,

	on_activate = function( self, staticdata, dtime_s )
		-- remove orphaned objects from world
		if dtime_s > 0 then
			self.object:remove( )
			return
		end

		self.object:set_armor_groups( { immortal = 1 } )

		HiddenCamera( self )
	end,

	get_staticdata = function ( self )
	end,
} )

--------------------------
-- Registered Callbacks --
--------------------------

minetest.register_on_joinplayer( function( player )
	local player_name = player:get_player_name( )
	
	if is_player_camera( player_name ) then
		local obj = minetest.add_entity( vector.origin, "spawncam:hologram" )
		camera = obj:get_luaentity( )
		camera:on_construct( player )
	end
end )

minetest.register_on_leaveplayer( function( player )
	local player_name = player:get_player_name( )

	if is_player_camera( player_name ) then
		camera:on_destruct( )
		camera.object:remove( )
		camera = nil
	end
end )

---------------------

spawncam = {
	viewpoints = config.viewpoints,
	is_player_camera = is_player_camera,
}

if not minetest.get_date_string or not minetest.get_time_string then
	dofile( mod_path .. "/compatibility.lua" )
end
