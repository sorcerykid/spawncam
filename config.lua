local pi = core.math.pi
local FACE_NORTH = 0
local FACE_SOUTH = pi
local FACE_EAST = -pi / 2
local FACE_WEST = pi / 2
local FACE_NORTHEAST = -pi / 4
local FACE_NORTHWEST = pi / 4
local FACE_SOUTHEAST = -pi / 4 * 3
local FACE_SOUTHWEST = pi / 4 * 3
local TILT_UPWARD = -pi / 4
local TILT_DOWNWARD = pi / 4
local TILT_FORWARD = 0

local function vec( x, y, z ) return { x = x, y = y, z = z } end

viewpoints = {
	{
		title = "Northeast",
		pos = vec( 8, 15, 13 ),
		pitch = TILT_DOWNWARD,
		yaw = FACE_SOUTHWEST,
		timeout = 2,
		on_exit = function ( self )
			self:jump_to_next( )
		end,
	},

	{
		title = "Southeast",
		pos = vec( 8, 15, -13 ),
		pitch = TILT_DOWNWARD,
		yaw = FACE_NORTHWEST,
		timeout = 2,
		on_exit = function ( self )
			self:jump_to_next( )
		end,
	},
}

