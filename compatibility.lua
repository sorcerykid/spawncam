function core.get_date_string( str, env_date )
	if not env_date then
		env_date = minetest.get_day_count( )
	end

	local months1 = { 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' }
	local months2 = { 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' }

	if str == nil then
		str = "%D Days, %M Months, %Y Years"
	end

	local y = math.floor( env_date / 360 )
	local m = math.floor( env_date % 360 / 30 )
	local d = math.floor( env_date % 360 % 30 )
	local j = math.floor( env_date % 360 )

	-- cardinal values
	str = string.gsub( str, "%%Y", y )	      -- elapsed years in epoch
	str = string.gsub( str, "%%M", m )	      -- elapsed months in year
	str = string.gsub( str, "%%D", d )	      -- elapsed days in month
	str = string.gsub( str, "%%J", j )	      -- elapsed days in year

	-- ordinal values
	str = string.gsub( str, "%%y", y + 1 )	  -- current year of epoch
	str = string.gsub( str, "%%m", m + 1 )	  -- current month of year
	str = string.gsub( str, "%%d", d + 1 )	  -- current day of month
	str = string.gsub( str, "%%j", j + 1 )	  -- current day of year

	str = string.gsub( str, "%%b", months1[ m + 1 ] )       -- current month long name
	str = string.gsub( str, "%%h", months2[ m + 1 ] )       -- current month short name

	str = string.gsub( str, "%%z", "%%" )

	return str
end

function core.get_time_string( str, env_time )
	if not env_time then
		env_time = math.floor( minetest.get_timeofday( ) * 1440 )
	elseif env_time > 1440 then
		env_time = env_time % 1440      -- prevent overflow
	end

	if str == nil then
		str = "%cc:%mm %pp"
	end

	local pp = env_time < 720 and "AM" or "PM"
	local hh = math.floor( env_time / 60 )
	local mm = math.floor( env_time % 60 )
	local cc = hh

	if cc > 12 then
		cc = cc - 12;
	elseif cc == 0 then
		cc = 12;
	end

	str = string.gsub( str, "%%pp", pp )			    -- daypart as AM or PM
	str = string.gsub( str, "%%hh", string.format( "%02d", hh ) )   -- hours in 24 hour clock
	str = string.gsub( str, "%%cc", string.format( "%02d", cc ) )   -- hours in 12 hour clock
	str = string.gsub( str, "%%mm", string.format( "%02d", mm ) )   -- minutes

	return str
end
